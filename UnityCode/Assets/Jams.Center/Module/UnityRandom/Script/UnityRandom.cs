﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;

public class UnityRandom  {


    public static float Random(float max = float.MaxValue, bool unsign = true)
    {
        return UnityEngine.Random.Range(unsign ? 0 : -max, max);
    }
    public static int Random(int max = int.MaxValue, bool unsign = true)
    {
        return UnityEngine.Random.Range(unsign ? 0 : -max, max);
    }
    public static int Random(string characters)
    {
        return characters[Random(characters.Length)];
    }
    public static int Random(char [] characters)
    {
        return characters[Random(characters.Length)];
    }

    public static float Range(float min, float max) { return UnityEngine.Random.Range(min, max); }
    public static float Range(int min, int max) { return UnityEngine.Random.Range(min, max); }

    public static float RandomPourcent() { return UnityEngine.Random.Range(0f, 1f); }
    public static Color RandomColor() { return new UnityEngine.Color(RandomPourcent(), RandomPourcent(), RandomPourcent()); }
    public static char RandomLetter() {return "abcdefghijklmnopqrstuvwxyz"[Random(26)];}

    public static Vector3 RandomEuleur() { return new Vector3(Random(360), Random(360), Random(360)); }
    public static Quaternion RandomRotation() { return Quaternion.Euler(RandomEuleur()); }
    public static Vector3 RandomDirection() { return RandomRotation() * Vector3.forward; }
    public static Vector3 RandomPosition(float range=1, bool spherical=true) {
        if (spherical)
            return RandomDirection()*Random(range);
        else
            return new Vector3(Random(range, false), Random(range, false), Random(range, false));
    }

    public static Texture2D RandomTexture(int width , int height , Color color )
    {
        Texture2D text = new Texture2D(width, height);
        Color[] pixels = text.GetPixels();

        for (int i = 0; i < width*height; i++)
        {
            pixels[i] = color;
        }
        text.SetPixels(pixels);
        text.Apply();
        return text;

    }
    public static Texture2D RandomTexture(int width, int height)
    {
        Texture2D text = new Texture2D(width, height);
        Color[] pixels = text.GetPixels();

        for (int i = 0; i < width * height; i++)
        {
            pixels[i] = RandomColor();
        }
        text.SetPixels(pixels);
        text.Apply();
        return text;

    }

    public static string RandomCode(int length, bool minor, bool major, bool number) {
        string characters = "";
        if(minor)
            characters += "abcdefghijklmnopqrstuvwxyz";
        if (major)
            characters += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if (number)
            characters += "0123456789";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++)
        {
            builder.Append(Random(characters));
        }
        return builder.ToString();
    }


}
